var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM account;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(account_id, callback) {
    var query = 'SELECT acc.*, sk.skill_name, s.school_name, c.company_name FROM account acc ' +
        'LEFT JOIN account_skill ask ON acc.account_id = ask.account_id ' +
        'LEFT JOIN skill sk ON ask.skill_id = sk.skill_id ' +
        'LEFT JOIN account school acs ON acc.account_id = acs.account_id ' +
        'LEFT JOIN school s ON as.school_id = s.school_id ' +
        'LEFT JOIN account_company ac ON acc.account_id = ac.account_id ' +
        'LEFT JOIN company c on ac.company_id = c.company_id ' +
        'WHERE acc.account_id = ?';
    var queryData = [account_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    var query = 'INSERT INTO account (email, first_name, last_name) VALUES (?, ?, ?)';

    var queryData = [params.email, params.first_name, params.last_name];

    connection.query(query, params.email, params.first_name, params.last_name, function(err, result) {
      var account_id = result.insertId;

      var query = 'INSERT INTO account_skill (account_id, skill_id) VALUES ?';

      var accountSkillData = [];
      if (params.skill_id.constructor === Array) {
          for (var i = 0; i < params.skill_id.length; i++) {
              accountSkillData.push([account_id, params.skill_id]);
          }
      }
      else {
          accountSkillData.push([account_id, params.skill_id]);
      }
      });

    connection.query(query, params.email, params.first_name, params.last_name, function(err, result) {
        var account_id = result.insertId;

        var query = 'INSERT INTO account_school (account_id, school_id) VALUES ?';

        var accountSchoolData = [];
        if (params.school_id.constructor === Array) {
            for (var i = 0; i < params.school_id.length; i++) {
                accountSchoolData.push([account_id, params.school_id]);
            }
        }
        else {
            accountSchoolData.push([account_id, params.school_id]);
        }
    });

    connection.query(query, params.email, params.first_name, params.last_name, function(err, result) {
        var account_id = result.insertId;

        var query = 'INSERT INTO account_company (account_id, company_id) VALUES ?';

        var accountCompanyData = [];
        if (params.company_id.constructor === Array) {
            for (var i = 0; i < params.company_id.length; i++) {
                accountCompanyData.push([account_id, params.company_id]);
            }
        }
        else {
            accountCompanyData.push([account_id, params.company_id]);
        }
    });

    connection.query(query, [accountSkillData], [accountSchoolData], [accountCompanyData], function(err, result) {
        callback(err, result);
    });
};

exports.delete = function(account_id, callback) {
    var query = 'DELETE FROM account WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

var accountSkillInsert = function(account_id, skillIdArray, callback){
    var query = 'INSERT INTO account_skill (account_id, skill_id) VALUES ?';

    var accountSkillData = [];
    if (skillIdArray.constructor === Array) {
        for (var i = 0; i < skillIdArray.length; i++) {
            accountSkillData.push([account_id, skillIdArray[i]]);
        }
    }
    else {
        accountSkillData.push([account_id, skillIdArray]);
    }
    connection.query(query, [accountSkillData], function(err, result){
        callback(err, result);
    });
};

module.exports.accountSkillInsert = accountSkillInsert;

var accountSkillDeleteAll = function(account_id, callback){
    var query = 'DELETE FROM account_skill WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

module.exports.accountSkillDeleteAll = accountSkillDeleteAll;

var accountSchoolInsert = function(account_id, schoolIdArray, callback){
    var query = 'INSERT INTO account_school (account_id, school_id) VALUES ?';

    var accountSchoolData = [];
    if (schoolIdArray.constructor === Array) {
        for (var i = 0; i < schooldArray.length; i++) {
            accountSchoolData.push([account_id, schoolIdArray[i]]);
        }
    }
    else {
        accountSchoolData.push([account_id, schoolIdArray]);
    }
    connection.query(query, [accountSchoolData], function(err, result){
        callback(err, result);
    });
};

module.exports.accountSchoolInsert = accountSchoolInsert;

var accountSchoolDeleteAll = function(account_id, callback){
    var query = 'DELETE FROM account_school WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

module.exports.accountSchoolDeleteAll = accountSchoolDeleteAll;

var accountCompanyInsert = function(account_id, companyIdArray, callback){
    var query = 'INSERT INTO account_company (account_id, company_id) VALUES ?';

    var accountCompanyData = [];
    if (companyIdArray.constructor === Array) {
        for (var i = 0; i < companyIdArray.length; i++) {
            accountCompanyData.push([account_id, companyIdArray[i]]);
        }
    }
    else {
        accountCompanyData.push([account_id, companyIdArray]);
    }
    connection.query(query, [accountCompanyData], function(err, result){
        callback(err, result);
    });
};

module.exports.accountCompanyInsert = accountCompanyInsert;

var accountCompanyDeleteAll = function(account_id, callback){
    var query = 'DELETE FROM account_company WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

module.exports.accountCompanyDeleteAll = accountCompanyDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE account set email = ?, first_name = ?, last_name = ? WHERE account_id = ?';
    var queryData = [params.email, params.first_name, params.last_name, params.account_id];

    connection.query(query, queryData, function(err, result) {
        accountSkillDeleteAll(params.account_id, function(err, result){
            if(params.skill_id != null) {
                accountSkillInsert(params.account_id, params.skill_id, function(err, result){
                    callback(err, result);
                });
            }
            else {
                callback(err, result);
            }
        })
    })

    connection.query(query, queryData, function(err, result) {
        accountSchoolDeleteAll(params.account_id, function(err, result){
            if(params.school_id != null){
                accountSchoolInsert(params.account_id, params.skill_id, function(err, result){
                    callback(err, result);
                });
            }
            else {
                callback(err, result);
            }
        })
    })

    connection.query(query, queryData, function (err, result) {
        accountCompanyDeleteAll(params.account_id, function (err, result) {
            if(params.company_id != null) {
                accountCompanyInsert(params.account_id, params.company_id, function(err, result){
                    callback(err, result);
                });
            }
            else{
                callback(err, result);
            }
        });
    });
};

exports.edit = function(account_id, callback) {
    var query = 'CALL account_getinfo(?)';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};